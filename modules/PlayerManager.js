var Player = require('./Player.js');

function PlayerManager() {
	this._list = {};
}

PlayerManager.prototype.get = function (id) {
	return this._list[id];
};

PlayerManager.prototype.create = function (id, ip, settings) {
	settings = settings || {};
	var player = new Player(id, ip, settings);
	this._list[id] = player;
	this._removeDuplicates(id, ip);
	this.getManager('tank').create(player, id);
};

PlayerManager.prototype._removeDuplicates = function (id, ip) {
	for (var i in this._list) {
		if (this._list[i].getIp() == ip && i != id) {
			this._list[id].addStats(this._list[i]);
			this.removePlayer(i);
			this.getManager('tank').removeTank(i);
			break;
		}
	}
};

PlayerManager.prototype.removePlayer = function (id) {
	delete this._list[id];
};

module.exports = exports = PlayerManager;