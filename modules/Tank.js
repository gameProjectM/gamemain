var abilities = require('./abilities.js');
var map = require('./Map.js');
var myMath = require('./Math.js');

var tankManager,
	specialObjectManager,
	playerManager,
	bulletManager;

// TODO: zmienne prywatne powinny być poprzedzone '_'
function Tank(manager, id, wsp) {
	tankManager = manager;
	this.x = wsp.x;
	this.y = wsp.y;
	this.speed = 10;
	this.dirX = 0;
	this.dirY = 0;
	this.radius = 22;
	this.r = 22;
	this.gunAngle = 0;
	this.id = id;
	this.life = 100;
	this.maxLife = 100;
	this.mx = null; // MOUSE X
	this.my = null; // MOUSE Y
	this.mPosX = this.x;
	this.mPosY = this.y + 1;
	this.Vx = 0;
	this.Vy = 0;
	this.auras = {};

	this._addAbilities();
	this._getManagers();
}

Tank.prototype.getAttr = function (attr) {
	return (attr in this) ?  this[attr] : new Error('wrong atttribute');
}

Tank.prototype._addAbilities = function () {
	this.ab = {};
	for (var i in abilities) {
		this.ab[i] = {
			amount: abilities[i].startAmount,
		}
	}
}

Tank.prototype._getManagers = function () {
	specialObjectManager = tankManager.getManager('specialObject');
	playerManager = tankManager.getManager('player');
	bulletManager = tankManager.getManager('bullet');
}

Tank.prototype.getStatsFromPlayer = function (player) {
	this.kills = player.getKills();
	this.deaths = player.getDeaths();
	this.nick = player.getNick();
}

Tank.prototype.getNick = function () {
	return this.nick;
}

Tank.prototype.takeDamage = function (dmg, maker) {
	console.log('dmg')
	this.life -= dmg;
	if (this.life <= 0)
		tankManager.destroy(maker.id, this.id);
}

Tank.prototype.useAbility = function (abilityName) {
	console.log('a');
	var ability = abilities[abilityName];
	if (this.ab[abilityName].amount > 0) {
		this.ab[abilityName].amount--;

		if (ability.bullets)
			bulletManager.create(this.id);

		if (ability.spObject)
			specialObjectManager.create(this.id, ability, this.mx, this.my);

		setTimeout(this._timeoutAbility.bind(this), ability.latency, {
			abilityName: abilityName,
			ability: ability,
			positionX: this.mx, // Istotne, żeby przenieść współrzędne myszki, 
			positionY: this.my  //	żeby po opóźnieniu pobrać współrzędne włąściwego punktu
		});
	}
}

Tank.prototype._timeoutAbility = function (parameters) {
	var ability = parameters.ability;
	var abilityName = parameters.abilityName;
	var positionX = parameters.positionX;
	var positionY = parameters.positionY;

	var dmg = ability.dmg;
	if (ability.AoE && dmg > 0) {
		var tanks = tankManager.getAll();
		for (var i in tanks) {
			var tank = tanks[i];
			if (!myMath.circleCollision(tank.x - positionX, tank.y - positionY, ability.radius))
				continue;
			tank.takeDamage(dmg, this);
		}
	}
	
	// TODO: to powinno być w innej klasie:
	tankManager.addAnimation({
		ab: abilityName,
		x: positionX, // MOUSE X
		y: positionY // MOUSE Y
	});
}

Tank.prototype.exportResources = function () {
	var res = {};
	for (var abilityName in this.ab) {
		var ability = this.ab[abilityName];
		res[abilities[abilityName].id] = {};
		for (var propertyName in ability) {
			res[abilities[abilityName].id][propertyName[0]] = ability[propertyName] // UWAGA - UCINA!!!!
		}
	}
	return res;
}

Tank.prototype._calculateSpeed = function() {
	var speed = this.speed;
	if (this.dirX != 0 && this.dirY != 0)
		speed /= Math.SQRT2;
		
	// Aura - speed boost
	if (this.auras.sb) {
		if (Date.now() < this.auras.sb.timeout) {
			speed *= 1.5;
		} else delete this.auras.sb;
	}
	
	// Czarna plama spowalniająca
	var maxOp = 0;
	var so = specialObjectManager.getAll();
	for (var i in so) {
		var ob = so[i];
		if (ob.kind == 'darkSpot') {
			if (myMath.circleCollision(ob.x - this.x, ob.y - this.y, ob.r + this.r)) {
				maxOp = Math.max(maxOp, ob.op);
			}
		}
	}
	speed *= ((2 - maxOp) / 2)
	return Math.round(speed);
}


Tank.prototype.move = function () {

	// TODO: Całość do podziału i zmiany...

	var r = this.radius;
	var x = this.x;
	var y = this.y;
	var dx = this.dirX;
	var dy = this.dirY;

	var speed = this._calculateSpeed();

	x += dx * speed;
	y += dy * speed;

	// Kolizje ze ścianami
	if (x < r) {
		x = r;
	} else if (x > map.width * map.tilewidth - r) {
		x = map.width * map.tilewidth - r;
	}
	if (y < r) {
		y = r;
	} else if (y > map.height * map.tileheight - r) {
		y = map.height * map.tileheight - r;
	}

	var cornersCoords = map.calculateCornerCoords(x, y, r);
	
	var d = map.layers[1].data;
	var b = {};
	for (var i = 0; i < cornersCoords.length; i++) {
		var tileContent = d[cornersCoords[i][0] + cornersCoords[i][1] * map.width]; 
		if (tileContent != 0) {
			b.x = cornersCoords[i][0];
			b.y = cornersCoords[i][1];
			b.x1 = b.x * map.tilewidth;
			b.y1 = b.y * map.tileheight;
			b.x2 = b.x * map.tilewidth + map.tilewidth;
			b.y2 = b.y * map.tileheight + map.tileheight;

			if (b.x1 < x + r && b.x2 > x - r && b.y1 < y + r && b.y2 > y - r) {
				var tile = cornersCoords[i][0] + cornersCoords[i][1] * map.width;

				switch (tileContent) {
					case 1:
						if (dx == -1) {
							if (Math.abs(b.x2 - (x - r)) <= Math.abs(dx * speed)) { // kolizja z prawym bokiem boxu
								x = b.x2 + r;
							}
						} else if (dx == 1) {
							if (Math.abs(b.x1 - (x + r)) <= Math.abs(dx * speed)) { // kolizja z lewym bokiem boxu
								x = b.x1 - r;
							}
						}
						if (dy == -1) {
							if (Math.abs(b.y2 - (y - r)) <= Math.abs(dy * speed)) { // kolizja z prawym bokiem boxu
								y = b.y2 + r;
							}
						} else if (dy == 1) {
							if (Math.abs(b.y1 - (y + r)) <= Math.abs(dy * speed)) { // kolizja z lewym bokiem boxu
								y = b.y1 - r;
							}
						}
						break;
					case 2:
						this.ab.shot.amount += myMath.randInt(5, 20);
						d[tile] = 0;
						map.changes.push([tile, 0]);
						break;
					case 3:
						this.ab.nuke.amount += myMath.randInt(1, 5);
						d[tile] = 0;
						map.changes.push([tile, 0]);
						break;
					case 4:
						if (this.life < this.maxLife) {
							this.life = Math.min(this.maxLife, this.life + 30);
							d[tile] = 0;
							map.changes.push([tile, 0]);
						}
						break;
					case 5:
						this.auras.sb = {
							timeout: Date.now() + 10000,
						}
						d[tile] = 0;
						map.changes.push([tile, 0]);
						break;
					case 6:
						this.ab.tarKeg.amount += myMath.randInt(1, 5);
						d[tile] = 0;
						map.changes.push([tile, 0]);
						break;
				}
			}
		}
	}
	
	this._updatePosition(x,y);
	this._updateCursorPosition();
	this._updateGunAngle();
}

Tank.prototype._updatePosition = function(x, y) {
	this.x = x;
	this.y = y;
}

Tank.prototype._updateCursorPosition = function() {
	this.mx = this.mPosX + this.x - playerManager.get(this.id).getScreenWidth() / 2;
	this.my = this.mPosY + this.y - playerManager.get(this.id).getScreenHeight() / 2;
}

Tank.prototype._updateGunAngle = function() {
	var v = new myMath.Vector(this.mx - this.x, this.my - this.y);
	this.gunAngle = v.angle;
}

module.exports = exports = Tank;