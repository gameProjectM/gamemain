function extend(dest, src) {
	dest.prototype = Object.create(src.prototype, {
		constructor: {
			value: dest.constructor,
			enumerable: false,
			writable: true,
			configurable: true
		}
	});
	dest.super_ = src;
}

module.exports = exports = {
	extend: extend
};

function clone(o) {
	return JSON.parse(JSON.stringify(o));
}