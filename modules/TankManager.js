var Tank = require('./Tank.js');
var myMath = require('./Math.js');
var map = require('./Map.js');

function TankManager() {
	this._list = {};
}

TankManager.prototype.get = function(id) {
	return this._list[id];
};

TankManager.prototype.removeTank = function(id) {
	delete this._list[id];
};

TankManager.prototype.getAll = function() {
	return this._list;
};

TankManager.prototype.move = function() {
	for (var id in this._list) {
		this._list[id].move();
	}
};

TankManager.prototype.create = function(player, id) {
	var x = myMath.randInt(0, 4);
	var wsp = map.getPos(map.startPositions[x])
	var t = new Tank(this, id, wsp);

	t.getStatsFromPlayer(player);
	this._list[id] = t;
};

TankManager.prototype.destroy = function(killerId, victimId) {
	delete this._list[victimId];
	if(killerId != victimId)
		this.getManager('player').get(killerId).addStats({_kills: 1})
	this.getManager('player').get(victimId).addStats({_deaths: 1});
}

TankManager.prototype.export = function() {
	var exportedObj = {};

	for (var tankId in this._list) {
		var tank = this._list[tankId];
		exportedObj[tankId] = {
			x: Math.round(tank.x),
			y: Math.round(tank.y),
			k: tank.kills,
			d: tank.deaths,
			g: tank.gunAngle * (50 / Math.PI) | 0, // <0, 2PI) -> {0,...,99}, dwa znaki
			lf: tank.life,
			mlf: tank.maxLife,
			ab: tank.exportResources(),
			n: tank.nick,
			a: tank.auras,
		}
	}
	return exportedObj;
}

module.exports = exports = TankManager;