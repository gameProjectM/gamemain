var map = require("./Map.js"),
	bulletManager,
	tankManager;

function Bullet(manager, id, id2) {

	bulletManager = manager;
	tankManager = manager.getManager('tank');

	var x = tankManager.get(id).getAttr('x');
	var y = tankManager.get(id).getAttr('y');

	var angle = tankManager.get(id).getAttr('gunAngle');

	this.sx = Math.cos(angle);
	this.sy = Math.sin(angle);

	this.x = x;
	this.y = y;

	this.id = id2;
	this.r = 5;
	this.owner = id;
	this.speed = 25;
}

Bullet.prototype.move = function () {
	this.x += this.sx * this.speed;
	this.y += this.sy * this.speed;

	if (this._isInCollisionWithWalls() || this._isInCollisionWithBoxes()) {
		bulletManager.remove(this.id);
		return;
	}

	// czy powinna być zrzucona odpowiedzialność??
	var tanks = tankManager.getAll();
	for (var id in tanks) {
		var tank = tanks[id];
		if (this.owner == id)
			continue;
		if (myMath.circleCollision(tank.x - this.x, tank.y - this.y, tank.r + this.r)) {
			bulletManager.remove(this.id);
			tank.getDamaged(10, this.owner);
			break;
		}
	}
}

Bullet.prototype._isInCollisionWithWalls = function () {
	return this.x < 0 || this.y < 0 || this.x > map.width * map.tilewidth || this.y > map.height * map.tileheight;
};

Bullet.prototype._isInCollisionWithBoxes = function () {
	return map.layers[1].data[map.getId(this.x, this.y)] == 1;
};

module.exports = exports = Bullet;