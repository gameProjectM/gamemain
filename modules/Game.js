var map = require('./Map.js'),
	SpecialObjectManager = require('./SpecialObjectManager.js'),
	BulletManager = require('./BulletManager.js'),
	TankManager = require('./TankManager.js'),
	PlayerManager = require('./PlayerManager.js');

var io,
	tankManager, 
	specialObjectManager, 
	bulletManager, 
	playerManager;

function Game(io) {
	_io = io; // get the connection
	this.packageNr = 0;
	this.murders = [];
	this.animations = [];
	this.sounds = [];
	this.latency = 0;
	this.serverTime = Date.now();
	this.mapReloadId = null;
	this.mainLoopId = null
	this.resourceLoopId = null;
	this._managers = {};
}

Game.prototype.addAnimation = function(animation) {
	this.animations.push(animation);
};

Game.prototype.getManager = function (type) {
	var manager = this._managers[type + 'Manager'];
	if (!manager)
		console.error('Manager: ' + type + ' not found');
	return manager;
}

Game.prototype.start = function () {
	this._initManagers();
	this._initLoops();
	this._startConnection();
}

Game.prototype._initManagers = function () {
	var m = this._managers;
	tankManager = m.tankManager = new TankManager();
	specialObjectManager = m.specialObjectManager = new SpecialObjectManager();
	bulletManager = m.bulletManager = new BulletManager();
	playerManager = m.playerManager = new PlayerManager();

	tankManager.getManager = this.getManager.bind(this);
	bulletManager.getManager = this.getManager.bind(this);
	playerManager.getManager = this.getManager.bind(this);
	specialObjectManager.getManager = this.getManager.bind(this);
	
	// TODO: do przeniesienia
	tankManager.addAnimation = this.addAnimation.bind(this);
}

Game.prototype._initLoops = function () {
	this.mapReloadId = setInterval(map.emit.bind(map, _io), 3000);
	this.mainLoopId = setInterval(this._mainLoop.bind(this), 25);
	this.resourceLoopId = setInterval(map.creatingResources.bind(map), 20000);
}

Game.prototype._mainLoop = function () {
	if (_io.engine.clientsCount > 0) {
		var time = Date.now();
		tankManager.move();
		bulletManager.move();
		specialObjectManager.animate();
		this.latency = Date.now() - time;
		this._sendData();
	}
}

Game.prototype._sendData = function () {
	var data = JSON.stringify({
		t: tankManager.export(),
		b: bulletManager.export(),
		mc: map.changes,
		sp: specialObjectManager.export(),
		a: this.animations,
		sl: this.latency,
		ts: Date.now(),
		m: this.murders,
		nr: ++this.packageNr,
	});
	_io.emit('game-update', data);
	map.changes.length = 0;
	this.animations.length = 0;
	this.murders.length = 0;
};

Game.prototype._startConnection = function() {
	var self = this;
	_io.on('connection', function (socket) {
		socket.on('disconnect', function () {
			var tank = tankManager.get(socket.id);
			if (tank)
				_io.emit('n-message', tank.getNick() + ' się rozłączył/-a');
			tankManager.removeTank(socket.id);
		});

		socket.on('game-ping', function (msg) {
			socket.emit('game-ping', JSON.stringify(msg))
		})

		socket.on('join-game', function (msg) {
			self._joinGame(socket, msg);
		});

		socket.on('reborn', function () {
			var player = playerManager.get(socket.id);
			if (player)
				tankManager.create(player, socket.id);
			socket.emit('join', JSON.stringify({
				map: map
			}));
		})

		socket.on('message', function (msg) {
			socket.broadcast.emit('message', msg);
		});
		socket.on('client-event', function (msg) {
			self._getClientEvent(socket, msg);
		});
	});
};

Game.prototype._joinGame = function(socket, data) {
	var msg = JSON.parse(data);
	socket.broadcast.emit('n-message', msg.nick + ' dołączył/-a do gry');
	socket.emit('n-message', 'Dołączyłeś/-aś do gry');

	playerManager.create(socket.id, socket.handshake.address, msg);

	socket.emit('join', JSON.stringify({
		width: map.width * map.tilewidth,
		height: map.height * map.tileheight,
		map: map
	}));
}

Game.prototype._getClientEvent = function(socket, msg) {
	var id = socket.id;
	if (!tankManager.get(id) || !id)
		return;

	for (var i in msg) {
		switch (i) {
			case 'mx':
				tankManager.get(id).mPosX = msg[i];
				break;
			case 'my':
				tankManager.get(id).mPosY = msg[i];
				break;
			case 'ability':
				setTimeout(tankManager.get(id).useAbility(msg[i]));
				// opóźnienie 0s w celu uzyskania najpierw pozycji myszki
				break;
			case 'dirX':
				tankManager.get(id).dirX = msg[i];
				break;
			case 'dirY':
				tankManager.get(id).dirY = msg[i];
				break;
			case 'sw':
				playerManager.get(id).setScreenWidth(msg[i]);
				break;
			case 'sh':
				playerManager.get(id).setScreenHeight(msg[i]);
				break;
			default:
				console.error(i, msg[i]);
				break;
		}
	}
}

module.exports = exports = Game;